package org.hspconsortium.bilirubin.cds.factory;

import org.hspconsortium.cdshooks.model.Card;
import org.hspconsortium.cdshooks.model.Indicator;
import org.hspconsortium.cdshooks.model.Link;
import org.hspconsortium.cdshooks.model.Source;

public class BilirubinCardFactory {

    private static Card helloWorldCard = new Card()
            .setSummary("Hello World Card")
            .setDetail("Hello World!")
            .setIndicator(Indicator.Info)
            .setSource(
                    new Source("Intermountain Healthcare", "https://intermountainhealthcare.org")
            );


    private static Card orderBilirubinLabCard = new Card()
            .setSummary("Order Bilirubin Lab")
            .setDetail(
                    "![](https://content.logicahealth.org/images/intermountain/logo/HLTH_H_CMYK_610x100.png)  " +
                            "\n" +
                            "Bilirubin levels have not been measured. Order a bilirubin lab."
            )
            .setSource(
                    new Source("Intermountain Healthcare", "https://intermountainhealthcare.org")
            )
            .setIndicator(Indicator.Warning);

    private static Card openBilirubinApplicationCard = new Card()
            .setSummary("Bilirubin Levels Exceed Threshold")
            .setDetail(
                    "![](https://content.logicahealth.org/images/intermountain/logo/HLTH_H_CMYK_610x100.png)  " +
                            "\n" +
                            "Bilirubin levels are exceeding risk levels.  Intervention is necessary."
            )
            .setIndicator(Indicator.Warning)
            .setSource(
                    new Source("Intermountain Healthcare", "https://intermountainhealthcare.org")
            )
            .addLink(
                    new Link("HSPC Bilirubin Risk Chart", "https://bilirubin-risk-chart.logicahealth.org/launch.html", Link.Type.Smart)
            );


    public enum CardType{
        info,
        suggestion,
        application
    }

    public static Card getCard(CardType type){
        switch(type){
            case info:
                return helloWorldCard;
            case suggestion:
                return orderBilirubinLabCard;
            case application:
                return openBilirubinApplicationCard;
            default:
                return helloWorldCard;
        }
    }
}
