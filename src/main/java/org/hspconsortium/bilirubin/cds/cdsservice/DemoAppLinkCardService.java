package org.hspconsortium.bilirubin.cds.cdsservice;

import org.hspconsortium.bilirubin.cds.factory.BilirubinCardFactory;
import org.hspconsortium.cdshooks.model.HookCatalog;
import org.hspconsortium.cdshooks.model.ServiceDefinition;
import org.hspconsortium.cdshooks.model.ServiceInvocation;
import org.hspconsortium.cdshooks.model.ServiceResponse;
import org.hspconsortium.cdshooks.service.ExecutableService;
import org.hspconsortium.cdshooks.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DemoAppLinkCardService  extends ServiceDefinition implements ExecutableService {
    @Override
    public ServiceResponse execute(ServiceInvocation serviceInvocation) {

        ServiceResponse serviceResponse = new ServiceResponse();
        serviceResponse.addCard(BilirubinCardFactory.getCard(BilirubinCardFactory.CardType.application));
        return serviceResponse;
    }

    @Autowired
    public DemoAppLinkCardService(ServiceRegistry serviceRegistry){
        this.setId("demo-app-link-card")
                .setHook(HookCatalog.PatientView)
                .setTitle("Bilirubin Sample Application Link Card")
                .setDescription("Sends a Bilirubin Sample Application Link Card");
        serviceRegistry.add(this);

    }
}
