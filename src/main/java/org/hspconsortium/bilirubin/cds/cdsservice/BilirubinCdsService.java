package org.hspconsortium.bilirubin.cds.cdsservice;

import org.hl7.fhir.dstu3.model.Bundle;
import org.hl7.fhir.dstu3.model.Observation;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Quantity;
import org.hspconsortium.bilirubin.cds.factory.BilirubinCardFactory;
import org.hspconsortium.cdshooks.converter.StringToResourceConverter;
import org.hspconsortium.cdshooks.model.*;
import org.hspconsortium.cdshooks.service.ExecutableService;
import org.hspconsortium.cdshooks.service.ServiceRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

@Component
public class BilirubinCdsService extends ServiceDefinition implements ExecutableService {

    private static final Date CHILD_MAX_DATE = new GregorianCalendar(2000, 0, 01).getTime();

    public static final double MAX_BILIRUBIN_VALUE = 12.0;

    @Autowired
    private StringToResourceConverter stringToResourceConverter;

    @Autowired
    public BilirubinCdsService(ServiceRegistry serviceRegistry) {
        this.setId("bilirubin-cds-hook")
                .setHook(HookCatalog.PatientView)
                .setTitle("Bilirubin CDS Hooks ExecutableService")
                .setDescription("Detects issues with infants and the Kernicterus risk protocol")
                .addPrefetch("patient", "Patient/{{context.patientId}}")
                .addPrefetch("observations", "Observation?patient={{context.patientId}}&code=http://loinc.org|58941-6&_sort:desc=date&_count=1");

        serviceRegistry.add(this);
    }

    @Override
    public ServiceResponse execute(ServiceInvocation serviceInvocation) {
        ServiceResponse serviceResponse = new ServiceResponse();
        Card card = bilirubinObservationRequiredProtocol(serviceInvocation);
        if (card != null) {
            serviceResponse.addCard(card);
        }
        return serviceResponse;
    }

    public Card bilirubinObservationRequiredProtocol(ServiceInvocation serviceInvocation) {
        Map<String, PrefetchInvocation> prefetchInvocationMap = serviceInvocation.getPrefetch();
        if (prefetchInvocationMap != null) {
            PrefetchInvocation prefetchPatient = prefetchInvocationMap.get("patient");
            if (prefetchPatient != null && "200 OK".equals(prefetchPatient.getResponseCode())) {
                Patient patient = stringToResourceConverter.toResource(prefetchPatient.getResource(), Patient.class);
                // this is just a huge approximate
                if (patient != null && patient.getBirthDate() != null && CHILD_MAX_DATE.before(patient.getBirthDate())) {
                    // yes, is a child
                    PrefetchInvocation prefetchObservations = prefetchInvocationMap.get("observations");
                    if (prefetchObservations != null && "200 OK".equals(prefetchObservations.getResponseCode())) {
                        Bundle observationsBundle = stringToResourceConverter.toResource(prefetchObservations.getResource(), Bundle.class);
                        List<Bundle.BundleEntryComponent> observations = observationsBundle.getEntry();
                        // should only be one because of the search
                        if (observations != null && !observations.isEmpty()) {
                            Bundle.BundleEntryComponent observationEntry = observations.get(0);
                            Observation mostRecentObservation = (Observation) (observationEntry.getResource());
                            if (mostRecentObservation.getValue() != null
                                    && mostRecentObservation.getValue() instanceof Quantity) {
                                Quantity observationValue = (Quantity) (mostRecentObservation.getValue());
                                if (observationValue.getValue() != null && MAX_BILIRUBIN_VALUE < observationValue.getValue().doubleValue()) {
                                    return BilirubinCardFactory.getCard(BilirubinCardFactory.CardType.application);
                                }
                            }
                            // no cards here for patient with observations
                            return null;
                        }
                    }
                    // patient without observations
                    return BilirubinCardFactory.getCard(BilirubinCardFactory.CardType.suggestion);
                }
            }
        }
        return null;
    }
}
